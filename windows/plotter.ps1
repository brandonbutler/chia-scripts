# Chia install location
$ChiaVer = "1.1.6"
$ChiaPath = "~\AppData\Local\Chia-Blockchain\app-$ChiaVer\resources\app.asar.unpacked\daemon"

# The path to a blank text file
# If the text file exists, the script will create a new plot
# If the text file does not exist, once the current plot is finished creating, it will not continue making a new one
# Useful for scheduling upgrades
$PlotCheckFile = "D:\chia\check"

# File for containing the length of time it took to finish a plot
$PlotLogFile = "D:\chia\log.txt"

# Plot creation values
$PlotFinalPath = "E:\chia\final"
$PlotTempPath = "D:\chia\tmp"
$KSize = "32"
$Threads = "2"
$Buckets = "128"
$Memory = "4400"
# Set this to "-e" to disable bitfield for the plotter process
$DisableBitfield = ""

function Validate-Settings {
    # Check Plotfile
    if (Test-Path -Path $PlotCheckFile -PathType Leaf){
        echo "Plot check-file exists! Continuing..."
    }else {
        echo "Plot check-file does not exist. Not plotting!"
        Read-Host -Prompt "Press Enter to exit."
        exit 1
    }

    # Check temp path
    if (Test-Path -Path $PlotTempPath){
        echo "Temp path exists!"
    }else {
        echo "Temp path does not exist, exiting!"
        Read-Host -Prompt "Press Enter to exit."
        exit 1
    }

    # Check final path
    if (Test-Path -Path $PlotFinalPath){
        echo "Final path exists!"
    }else {
        echo "Final path does not exist, exiting!"
        Read-Host -Prompt "Press Enter to exit."
        exit 1
    }

    # Check Chia path
    if (Test-Path -Path $ChiaPath){
        echo "Chia installation path exists!"
    }else {
        echo "Chia installation path does not exist, exiting!"
        Read-Host -Prompt "Press Enter to exit."
        exit 1
    }
}

function Plot {
    cd $ChiaPath
    $performance = Measure-Command -Expression { 
        ./chia plots create $DisableBitfield -k $KSize -r $Threads -u $Buckets -b $Memory -t $PlotTempPath -d $PlotFinalPath | Write-Host
    }
    Add-Content $PlotLogFile $performance
}

# Script starts here
echo "----------------------------------------------"
echo "Chia path is $ChiaPath"
echo "Plot check-file is $PlotCheckFile"
echo "----------------------------------------------"
echo ""

while($true)
{
    Validate-Settings
    Plot
}
