Install-Module -Name powershell-yaml -Force -Verbose -Scope CurrentUser
Import-Module powershell-yaml

# Harvester vars (CHANGE THESE)
$CHIA_DIR = "~\AppData\Local\Chia-Blockchain\app-$LOCAL_CHIA_VERSION\resources\app.asar.unpacked\daemon"
$CHIA_CONFIG_PATH = "~\.chia\mainnet\config\config.yaml"
# Harvester vars cont. (Leave alone)
$LOCAL_TEMP_CA_DIR = "C:\temp\chia\ca"
$LOCAL_CHIA_VERSION = "1.1.6"

# Remote farmer vars (CHANGE THESE)
$FARMER_USER = "my-user"
$FARMER_IP = "192.168.1.111"
# Remote farmer vars cont. (Leave alone)
$FARMER_CA_DIR = "/home/$FARMER_USER/.chia/mainnet/config/ssl/ca"

# LoadYml function that will read YML file and deserialize it
function LoadYml {
    param (
        $FileName
    )
    # Load file content to a string array containing all YML file lines
    [string[]]$fileContent = Get-Content $FileName
    $content = ''
    # Convert a string array to a string
    foreach ($line in $fileContent) { $content = $content + "`n" + $line }
    # Deserialize a string to the PowerShell object
    $yml = ConvertFrom-YAML $content
    # return the object
    Write-Output $yml
}

# WriteYml function that writes the YML content to a file
function WriteYml {
    param (
        $FileName,
        $Content
    )
    #Serialize a PowerShell object to string
    $result = ConvertTo-YAML $Content
    #write to a file
    Set-Content -Path $FileName -Value $result
}

# Delete local CA directory if it exists and pull the new copy
Remove-Item ${LOCAL_TEMP_CA_DIR} -Recurse -ErrorAction Ignore
mkdir ${LOCAL_TEMP_CA_DIR}
scp ${FARMER_USER}@${FARMER_IP}:${FARMER_CA_DIR}/* ${LOCAL_TEMP_CA_DIR}

# Init chia and remove temp dir CA certs
& ${CHIA_DIR}\chia stop all -d
& ${CHIA_DIR}\chia init -c ${LOCAL_TEMP_CA_DIR}
Remove-Item ${LOCAL_TEMP_CA_DIR} -Recurse -ErrorAction Ignore

# Loading yml, setting new values and writing it back to disk
$yml = LoadYml ${CHIA_CONFIG_PATH}
$yml.harvester.farmer_peer.host = ${FARMER_IP}
WriteYml ${CHIA_CONFIG_PATH} ${yml}

# Start harvester
& ${CHIA_DIR}\chia start harvester -r

Read-Host -Prompt "Press Enter to Exit"
