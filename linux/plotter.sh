#!/bin/bash

chiapath="$HOME/chia-blockchain"
plotfinalpath=/mnt/final1
plottemppath=/mnt/tmp1
plotcheckfile="$HOME/chia/check"
plotlogfile="$HOME/chia/output.txt"

ksize=32
threads=4
buckets=128
ram=6750

echo "----------------------------------------------"
echo "chiapath is $chiapath"
echo "plotcheckfile is $plotcheckfile"
echo "----------------------------------------------"
echo ""

validateAndActivate() {
    if [ -d "$plottemppath" ]; then
        echo "Temp path exists..."
    else
        echo "Temp path does not exist! Exiting!"
        exit 1
    fi
    if [ -d "$plotfinalpath" ]; then
        echo "Final path exists..."
    else
        echo "Final path does not exist! Exiting!"
        exit 1
    fi
    if [ -d "$chiapath" ]; then
        echo "Found chia installation..."
    else
        echo "Chia installation path does not exist! Exiting!"
        exit 1
    fi
    cd $chiapath || exit 1
    # shellcheck disable=SC1091
    . ./activate
}

plot() {
    echo "Creating plot with k=$ksize buckets=$buckets RAM=$ram threads=$threads"
    start=$SECONDS
    chia plots create -k $ksize -r $threads -u $buckets -b $ram -t $plottemppath -d $plotfinalpath
    duration=$(( SECONDS - start ))
    echo "Plot took $duration seconds to complete"
    echo "$(( duration / 60 )) minutes" >> $plotlogfile
}

while : ; do
    validateAndActivate
    if [ -f "$plotcheckfile" ]; then
        echo "plot file exists! Continuing..."
        plot
    else
        echo "$plotcheckfile does not exist. Not plotting!"
        exit 0
    fi
done
