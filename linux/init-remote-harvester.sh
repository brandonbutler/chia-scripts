#!/bin/bash

# Local Chia source path, config path, and path to save remote farmer CA certs
CHIA_DIR="$HOME/chia-blockchain"
CHIA_CONFIG_PATH="$HOME/.chia/mainnet/config/config.yaml"
CA_DIR_LOCAL="$HOME/.chia-ca"

# Remote farmer username, hostname, IP for harvester config, and remote CA directory
FARMER_USER="my-user"
FARMER_IP="192.168.1.111"
CA_DIR_FARMER="/home/bb/.chia/mainnet/config/ssl/ca"

# Collect CA certs from remote farmer
rm -r ${CA_DIR_LOCAL} || true
mkdir -p ${CA_DIR_LOCAL}
scp ${FARMER_USER}@${FARMER_IP}:${CA_DIR_FARMER}/* ${CA_DIR_LOCAL}

# Activate chia venv
cd ${CHIA_DIR}
. ./activate

# Init chia with remote farmer CA certs
chia stop all -d
chia init -c ${CA_DIR_LOCAL}

# Update config.yaml to point to the remote farmer IP
yq e '.harvester.farmer_peer.host = "'$FARMER_IP'"' -i "$CHIA_CONFIG_PATH"

# Start harvester
chia start harvester -r
read -p "Press any key to shut down this script, it should be left open in a screen/tmux session" x
