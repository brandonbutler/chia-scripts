# chia-scripts

This project contains scripts for creating chia plots, as well as initializing remote harvesters on Windows/Linux machines. All scripts should work out of the box by just changing the variables found at the top of the script, using the direction provided in comments.

## Plotter scripts

These are meant to be ran several times, either in tmux/screen on Linux machines, or just several powershell windows on Windows. After a plot is created, the script will write to a logfile specifying how long it took to create the plot. 

Additionally, there is a "check" file that is just an empty text file that tells the script whether or not to proceed at the next iteration. Otherwise, plots will be created endlessly. This is useful for scheduling upgrades, for example, or starting the script on a new final directory because the old final drive was filled.

## Remote harvester init scripts

This is meant to be ran once on the remote harvester in question, either in tmux/screen on Linux machines, or just a powershell window on Windows.  It will handle copying the CA cert directory over from your farmer to your remote harvester, initializing Chia with the farmer's CA certs, and adding your farmer's IP address to your harvester config.yaml. 

On linux, the only dependency is the `yq` utility. A similar yaml parser utility is installed for you in the Windows version of the script.
